package id.bima.diver.gateway.config;

import java.lang.annotation.Annotation;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.ObjectProvider;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpHeaders;
import org.springframework.util.CollectionUtils;
import org.springframework.web.method.HandlerMethod;
import org.springframework.web.servlet.HandlerInterceptor;

import id.bima.diver.common.constant.HeaderKey;
import id.bima.diver.common.dto.BaseResponse;
import id.bima.diver.common.util.BaseResponseUtils;
import id.bima.diver.gateway.annotation.UserAuthenticate;
import id.bima.diver.gateway.authentication.dto.ValidateRequestDto;
import id.bima.diver.gateway.authentication.dto.ValidateResponseDto;
import id.bima.diver.gateway.authentication.service.AuthClient;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Configuration
public class AuthInterceptor implements HandlerInterceptor {
	
	private ObjectProvider<AuthClient> authClient;
	
	@Autowired
	public AuthInterceptor(ObjectProvider<AuthClient> authClient) {
		this.authClient = authClient;
	}

	@Override
	public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
		if (!(handler instanceof HandlerMethod)) {
            log.info("Wrong type of HandlerInterceptor");
            return true;
        }
		
		List<String> listTypeRequired = new ArrayList<>();
		HandlerMethod hm = (HandlerMethod) handler;
        for (Annotation annotation : hm.getMethod().getDeclaredAnnotations()) {
        	UserAuthenticate userAuthenticate = annotation.annotationType().getAnnotation(UserAuthenticate.class);
        	if (userAuthenticate != null) {
        		listTypeRequired.add(userAuthenticate.getRequiredType());
        	}
		}
        
        if (CollectionUtils.isEmpty(listTypeRequired)) {
        	return true;
        }
        
        ValidateRequestDto validateReq = ValidateRequestDto.builder()
        		.token(request.getHeader(HttpHeaders.AUTHORIZATION))
        		.url(request.getRequestURI())
        		.listRequiredType(listTypeRequired)
        		.build();
        BaseResponse<ValidateResponseDto> validateResponse = authClient.getObject().validate(validateReq);
        BaseResponseUtils.checkResponse(validateResponse);
        if (BaseResponseUtils.isSuccess(validateResponse)) {
        	ValidateResponseDto responseDto = validateResponse.getData();
        	request.setAttribute(HeaderKey.USER_ID, responseDto.getUserId());
        }
		
		return true;
	}
	
}
