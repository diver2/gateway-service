package id.bima.diver.gateway.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

@Configuration
@EnableWebMvc
public class InitConfiguration implements WebMvcConfigurer {
	
	private EndpointInterceptor endpointInterceptor;
	
	@Autowired
	public InitConfiguration(EndpointInterceptor endpointInterceptor) {
		this.endpointInterceptor = endpointInterceptor;
	}

	@Override
	public void addInterceptors(InterceptorRegistry registry) {
		registry.addInterceptor(endpointInterceptor);
	}
	
}
