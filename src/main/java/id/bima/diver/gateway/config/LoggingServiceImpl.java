package id.bima.diver.gateway.config;

import java.util.Collection;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Map;

import org.springframework.stereotype.Service;

import id.bima.diver.common.exception.GenericException;
import id.bima.diver.common.util.JsonUtils;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class LoggingServiceImpl implements LoggingService {

	@Override
	public void logRequest(HttpServletRequest httpServletRequest, Object body) {
		StringBuilder stringBuilder = new StringBuilder();
		try {
			Map<String, String> parameters = buildParametersMap(httpServletRequest);
			stringBuilder.append("REQUEST ");
			stringBuilder.append("[METHOD = ").append(httpServletRequest.getMethod()).append("] ");
			stringBuilder.append("[PATH = ").append(httpServletRequest.getRequestURI()).append("] ");
			stringBuilder.append("[HEADER = ").append(JsonUtils.objectAsStringJson(buildHeadersMap(httpServletRequest))).append("] ");
			if (!parameters.isEmpty()) {
				stringBuilder.append("[PARAMETER = ").append(JsonUtils.objectAsStringJson(parameters)).append("] ");
			}
			if (body != null) {
				stringBuilder.append("[BODY = ").append(JsonUtils.objectAsStringJson(body)).append("] ");
			}
		} catch (GenericException e) {
			log.error("There is an error where parsing request");
		}

		log.info(stringBuilder.toString());
	}

	@Override
	public void logResponse(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse, Object body) {
		StringBuilder stringBuilder = new StringBuilder();
		try {
			stringBuilder.append("RESPONSE ");
			stringBuilder.append("[METHOD = ").append(httpServletRequest.getMethod()).append("] ");
			stringBuilder.append("[PATH = ").append(httpServletRequest.getRequestURI()).append("] ");
			stringBuilder.append("[HEADER = ").append(JsonUtils.objectAsStringJson(buildHeadersMap(httpServletResponse))).append("] ");
			stringBuilder.append("[BODY = ").append(JsonUtils.objectAsStringJson(body)).append("] ");
		} catch (GenericException e) {
			log.error("There is an error where parsing response");
		}

		log.info(stringBuilder.toString());
	}

	private Map<String, String> buildParametersMap(HttpServletRequest httpServletRequest) {
		Map<String, String> resultMap = new HashMap<>();
		Enumeration<String> parameterNames = httpServletRequest.getParameterNames();

		while (parameterNames.hasMoreElements()) {
			String key = parameterNames.nextElement();
			String value = httpServletRequest.getParameter(key);
			resultMap.put(key, value);
		}

		return resultMap;
	}

	private Map<String, String> buildHeadersMap(HttpServletRequest request) {
		Map<String, String> map = new HashMap<>();

		Enumeration<String> headerNames = request.getHeaderNames();
		while (headerNames.hasMoreElements()) {
			String key = headerNames.nextElement();
			String value = request.getHeader(key);
			map.put(key, value);
		}

		return map;
	}

	private Map<String, String> buildHeadersMap(HttpServletResponse response) {
		Map<String, String> map = new HashMap<>();

		Collection<String> headerNames = response.getHeaderNames();
		for (String header : headerNames) {
			map.put(header, response.getHeader(header));
		}

		return map;
	}

}