package id.bima.diver.gateway.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import id.bima.diver.common.dto.BaseResponse;
import id.bima.diver.gateway.authentication.constant.AuthPath;
import id.bima.diver.gateway.authentication.dto.LoginRequestDto;
import id.bima.diver.gateway.authentication.dto.LoginResponseDto;
import id.bima.diver.gateway.authentication.dto.LogoutRequestDto;
import id.bima.diver.gateway.authentication.dto.LogoutResponseDto;
import id.bima.diver.gateway.authentication.service.AuthClient;
import io.swagger.v3.oas.annotations.Operation;

@RestController
@RequestMapping(AuthPath.AUTH_V1)
public class AuthController {
	
	private AuthClient authClient;
	
	@Autowired
	public AuthController(AuthClient authClient) {
		this.authClient = authClient;
	}

	@Operation(summary = "API to get token for main application")
	@PostMapping(value = AuthPath.MAIN_LOGIN, produces = { MediaType.APPLICATION_JSON_VALUE }, consumes = {MediaType.APPLICATION_JSON_VALUE })
	public BaseResponse<LoginResponseDto> mainLogin(@RequestBody LoginRequestDto request) {
		return authClient.mainLogin(request);
	}
	
	@Operation(summary = "API to get token for admin application")
	@PostMapping(value = AuthPath.ADMIN_LOGIN, produces = { MediaType.APPLICATION_JSON_VALUE }, consumes = {MediaType.APPLICATION_JSON_VALUE })
	public BaseResponse<LoginResponseDto> adminLogin(@RequestBody LoginRequestDto request) {
		return authClient.adminLogin(request);
	}
	
	@Operation(summary = "API to revoke token")
	@PostMapping(value = AuthPath.LOGOUT, produces = { MediaType.APPLICATION_JSON_VALUE }, consumes = {MediaType.APPLICATION_JSON_VALUE })
	public BaseResponse<LogoutResponseDto> logout(@RequestBody LogoutRequestDto request) {
		return authClient.logout(request);
	}
	
	
}
