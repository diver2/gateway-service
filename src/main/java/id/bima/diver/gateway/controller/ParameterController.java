package id.bima.diver.gateway.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import id.bima.diver.common.dto.BaseResponse;
import id.bima.diver.gateway.annotation.AdminUserAuthenticate;
import id.bima.diver.gateway.annotation.MainUserAuthenticate;
import id.bima.diver.gateway.parameter.constant.ParameterPath;
import id.bima.diver.gateway.parameter.dto.GetParameterRequestDto;
import id.bima.diver.gateway.parameter.dto.GetParameterResponseDto;
import id.bima.diver.gateway.parameter.service.ParameterClient;
import io.swagger.v3.oas.annotations.Operation;

@RestController
@RequestMapping(ParameterPath.PARAMETER_V1)
public class ParameterController {
	
	private ParameterClient parameterClient;
	
	@Autowired
	public ParameterController(ParameterClient parameterClient) {
		this.parameterClient = parameterClient;
	}

	@MainUserAuthenticate
	@AdminUserAuthenticate
	@Operation(summary = "API to get parameter from database")
	@PostMapping(value = ParameterPath.GET_BY_MODULE_AND_KEY, produces = { MediaType.APPLICATION_JSON_VALUE }, consumes = {MediaType.APPLICATION_JSON_VALUE })
	public BaseResponse<GetParameterResponseDto> getByModuleAndKey(@RequestBody GetParameterRequestDto request) {
		return parameterClient.getByModuleAndKey(request);
	}
	
	@MainUserAuthenticate
	@AdminUserAuthenticate
	@Operation(summary = "API to get parameter list from database")
	@PostMapping(value = ParameterPath.GET_LIST_BY_MODULE, produces = { MediaType.APPLICATION_JSON_VALUE }, consumes = {MediaType.APPLICATION_JSON_VALUE })
	public BaseResponse<GetParameterResponseDto> getListByModule(@RequestBody GetParameterRequestDto request) {
		return parameterClient.getListByModule(request);
	}
	
}
