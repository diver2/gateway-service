package id.bima.diver.gateway.annotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@UserAuthenticate(getRequiredType = "MAIN")
@Target(ElementType.METHOD)
@Retention(RetentionPolicy.RUNTIME)
public @interface MainUserAuthenticate {
}
