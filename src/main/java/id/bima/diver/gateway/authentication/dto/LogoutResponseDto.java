package id.bima.diver.gateway.authentication.dto;

import java.io.Serializable;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class LogoutResponseDto implements Serializable {

	private static final long serialVersionUID = -2294930740592870555L;
	
	private Boolean isSuccess;
	
}
