package id.bima.diver.gateway.authentication.dto;

import java.io.Serializable;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class ValidateResponseDto implements Serializable {

	private static final long serialVersionUID = 1504150777535128875L;
	
	private Long userId;
	
}
