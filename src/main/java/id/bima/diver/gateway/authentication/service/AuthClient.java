package id.bima.diver.gateway.authentication.service;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.context.annotation.Lazy;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

import id.bima.diver.common.dto.BaseResponse;
import id.bima.diver.gateway.authentication.constant.AuthPath;
import id.bima.diver.gateway.authentication.dto.LoginRequestDto;
import id.bima.diver.gateway.authentication.dto.LoginResponseDto;
import id.bima.diver.gateway.authentication.dto.LogoutRequestDto;
import id.bima.diver.gateway.authentication.dto.LogoutResponseDto;
import id.bima.diver.gateway.authentication.dto.ValidateRequestDto;
import id.bima.diver.gateway.authentication.dto.ValidateResponseDto;
import io.swagger.v3.oas.annotations.Operation;

@Lazy
@FeignClient(value = AuthPath.AUTH_V1, url = "${url.auth-service:}" + AuthPath.AUTH_V1)
public interface AuthClient {

	@Operation(summary = "API to get token for main application")
	@PostMapping(value = AuthPath.MAIN_LOGIN, produces = { MediaType.APPLICATION_JSON_VALUE }, consumes = {MediaType.APPLICATION_JSON_VALUE })
	public BaseResponse<LoginResponseDto> mainLogin(@RequestBody LoginRequestDto request);
	
	@Operation(summary = "API to get token for admin application")
	@PostMapping(value = AuthPath.ADMIN_LOGIN, produces = { MediaType.APPLICATION_JSON_VALUE }, consumes = {MediaType.APPLICATION_JSON_VALUE })
	public BaseResponse<LoginResponseDto> adminLogin(@RequestBody LoginRequestDto request);
	
	@Operation(summary = "API to validate token")
	@PostMapping(value = AuthPath.VALIDATE, produces = { MediaType.APPLICATION_JSON_VALUE }, consumes = {MediaType.APPLICATION_JSON_VALUE })
	public BaseResponse<ValidateResponseDto> validate(@RequestBody ValidateRequestDto request);
	
	@Operation(summary = "API to revoke token")
	@PostMapping(value = AuthPath.LOGOUT, produces = { MediaType.APPLICATION_JSON_VALUE }, consumes = {MediaType.APPLICATION_JSON_VALUE })
	public BaseResponse<LogoutResponseDto> logout(@RequestBody LogoutRequestDto request);
	
}
