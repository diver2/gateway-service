package id.bima.diver.gateway.authentication.dto;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class LoginResponseDto implements Serializable {
	
	private static final long serialVersionUID = -6464592482733456181L;
	
	private String token;
	private UserDto user;
	private List<RoleDto> listRole;
	private List<UserGroupDto> listUserGroup;
	private transient Map<String, Object> additionalData;
	
}
