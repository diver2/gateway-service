package id.bima.diver.gateway.authentication.constant;

public class AuthPath {
	
	private AuthPath() {
	}
	
	// Main Path
	public static final String AUTH_V1 = "v1/auth";

	// Parameter Controller Sub-Path
	public static final String ADMIN_LOGIN = "/admin-login";
	public static final String LOGOUT = "/logout";
	public static final String MAIN_LOGIN = "/main-login";
	public static final String VALIDATE = "/validate";

}
