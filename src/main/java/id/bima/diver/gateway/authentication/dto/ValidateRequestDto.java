package id.bima.diver.gateway.authentication.dto;

import java.io.Serializable;
import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class ValidateRequestDto implements Serializable {
	
	private static final long serialVersionUID = -3972555068290663722L;
	
	private String token;
	private String url;
	private List<String> listRequiredType;
	
}
