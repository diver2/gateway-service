package id.bima.diver.gateway.parameter.service;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.context.annotation.Lazy;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

import id.bima.diver.common.dto.BaseResponse;
import id.bima.diver.gateway.parameter.constant.ParameterPath;
import id.bima.diver.gateway.parameter.dto.GetParameterRequestDto;
import id.bima.diver.gateway.parameter.dto.GetParameterResponseDto;
import io.swagger.v3.oas.annotations.Operation;

@Lazy
@FeignClient(value = ParameterPath.PARAMETER_V1, url = "${url.parameter-service:}" + ParameterPath.PARAMETER_V1)
public interface ParameterClient {

	@Operation(summary = "API to get parameter from database")
	@PostMapping(value = ParameterPath.GET_BY_MODULE_AND_KEY, produces = { MediaType.APPLICATION_JSON_VALUE }, consumes = {MediaType.APPLICATION_JSON_VALUE })
	public BaseResponse<GetParameterResponseDto> getByModuleAndKey(@RequestBody GetParameterRequestDto request);
	
	@Operation(summary = "API to get parameter list from database")
	@PostMapping(value = ParameterPath.GET_LIST_BY_MODULE, produces = { MediaType.APPLICATION_JSON_VALUE }, consumes = {MediaType.APPLICATION_JSON_VALUE })
	public BaseResponse<GetParameterResponseDto> getListByModule(@RequestBody GetParameterRequestDto request);
	
}
